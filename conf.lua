function love.conf(t)
  t.window.title = "Destructive Dark"

  t.window.width = 800
  t.window.height = 600

  -- use display 2 if there's more than 1, usually that means
  -- we're on a laptop plugged into a monitor
  t.window.display = 2

  t.window.vsync = false
end
