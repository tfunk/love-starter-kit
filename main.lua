function love.load()
  _G['game_title'] = love.window.getTitle()

  -- Load up some libraries probably
  LightWorld = require 'lib.light-world'
  Camera     = require 'lib.camera'
  Timer      = require 'lib.timer'
  Class      = require 'lib.30log'
  Cargo      = require 'lib.cargo'
  Lume       = require 'lib.lume'
  Tiny       = require 'lib.tiny'
  Vec        = require 'lib.vector'

end

function love.draw()
  local w,h = love.graphics.getDimensions()
  love.graphics.polygon('fill', 100, 100, w-100, 100, w/2, h-100)
end

function love.update(dt)
  local fps = love.timer.getFPS()
  love.window.setTitle(string.format("%s -- (%d fps)", game_title, fps))
end

function love.keypressed(k, s, r)
  if s == 'escape' then
    love.event.quit()
  end
end
